__Bot de Telegram que anonimiza mensajes privados que le envía la gente y
los reenvía hacia un grupo o canal destino configurado.__

Solo se permitirá usar el bot a usuarixs que pertenezcan a un determinado grupo de telegram
(privado a público). Esto se debe configurar.

Cualquier mensaje que se le envíe al bot por privado, sea con imágenes, videos, archivos,
etc., será enviado exactamente tal cual hacia el grupo o canal destino configurado.

Este código está basado (un poco) en https://gitlab.com/rlyehlab/telegram-bot-priv-msgs/

Los comandos (en formato para pasarle a [@BotFather](https://telegram.me/botfather)) son:
> start - Comenzar a chatear con el bot   
> cancelar - Cancelar envío de mensaje   
> activar - [admins] Activa el bot (por defecto prendido)   
> desactivar - [admins] Desactiva el bot   

### Configuración

Las variables del `.env` (y de docker) son:
- `BOT_TOKEN` es el token del bot, en el formato NÚMERO:LETRAS_Y_SÍMBOLOS
- `ANONI_CHANNEL` canal o grupo al que reenviará los mensajes, en formato `@grupo`
- `USERS_GROUP` id del grupo al que deben pertenecer lxs usuarixs para poder usar el bot, en formato numérico (suele empezar con -100)
- `SUDO_USERS` listado de users admins, en formato `@user1,@user2` (¡sin espacios!)

Para averiguar el id de un grupo privado pueden hacer https://stackoverflow.com/questions/72640703/telegram-how-to-find-group-chat-id

### Corriendo el bot en local

Copiar `.env.example` a `.env` y cambiar valores acorde.

Primera corrida:
```
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
python3 bot.py
```

Posteriores corridas:
```
source venv/bin/activate
python3 bot.py
```

Testear docker:
```
sudo docker build -t tg-bot-anoniwall .
sudo docker run --rm tg-bot-anoniwall
```

### CI/CD

Para el CI/CD de gitlab configurar las variables `DEPLOY_HOST`, `SSH_PRIVATE_KEY`
y todas las de entorno del bot. Esto se puede hacer desde `Settings > CI/CD > Variables`
del repo de gitlab.

