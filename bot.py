#!/usr/bin/env python
# pylint: disable=unused-argument

import sys, os, logging
from pprint import pprint

# github - https://github.com/python-telegram-bot/python-telegram-bot
# code snippets - https://github.com/python-telegram-bot/python-telegram-bot/wiki/Code-snippets
# reference - https://python-telegram-bot.readthedocs.io/
from telegram import Update
from telegram.ext import (
    Application,
    CommandHandler,
    ContextTypes,
    ConversationHandler,
    MessageHandler,
    filters,
)
from telegram.error import BadRequest, Forbidden

from dotenv import load_dotenv
load_dotenv(verbose=True)

# Enable logging
logging.basicConfig(
    format="%(asctime)s - %(levelname)s - %(name)s - %(message)s", level=logging.INFO
)
# set higher logging level for httpx to avoid all GET and POST requests being logged
logging.getLogger("httpx").setLevel(logging.WARNING)

logger = logging.getLogger(__name__)

def try_get(KEY):
    value=os.getenv(KEY)
    if not value:
        print('Falta proporcionar:', KEY)
        sys.exit(1)
    return value

BOT_TOKEN = try_get("BOT_TOKEN")
ANONI_CHANNEL = try_get("ANONI_CHANNEL")
SUDO_USERS = try_get("SUDO_USERS").split(",")
USERS_GROUP = try_get("USERS_GROUP")

LADATA, CONFIRMA = range(2)

async def start(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    chat = update.effective_chat
    if chat.type != 'private':
        await update.message.reply_text(
			'Holis estoy acá solo para verificar quien me puede usar y quien no. '
			f'Para utilizarme hablame por privado a {context.bot.name}, gracias.'
        )
        return ConversationHandler.END
    await update.message.reply_text(
        f"¡Hola ser humanx! Todo lo que me escribas o envíes va a ser reenviado a {ANONI_CHANNEL}.\n\n"
        "Podés enviarme cualquier cosa 💁: links, imágenes, texto formateado, stickers, videos, archivos, etc.\n\n"
        "Además, siempre te voy a pedir confirmación antes de enviar tu mensaje.\n\n"
        "Ahora ... la data ... 🫦 ¡dámela!"
    )
    return LADATA

async def desactivar(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
	context.bot_data["activado"]=False
	await update.message.reply_text("⏸ Bot desactivado.")
	return LADATA
    
async def activar(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
	context.bot_data["activado"]=True
	await update.message.reply_text("✅ ¡Bot activado!")
	return LADATA

async def ladata(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
	# Chequiamos que el bot este activado
	if not context.bot_data["activado"]:
		await update.message.reply_text(
			"Lo siento me encuentro temporalmente desactivado. Volvé a probarme en unos minutos. "
			"Si sigo así por más tiempo considerá avisar a unx administradorx. Gracias."
		)
		return LADATA
		
	# Chequiamos que el usuarie pertenezca al grupo de usuarixs
	user = update.effective_user
	try:
		# ChatMember: https://docs.python-telegram-bot.org/en/stable/telegram.chatmember.html#telegram.ChatMember
		chat_member = await context.bot.get_chat_member(USERS_GROUP, user.id)
	except BadRequest as e:
		if str(e) == 'User not found':
			await update.message.reply_text(
				"Lo siento debés pertenecer al grupo de usuarixs para poder enviar mensajes. "
				"Si creés que esto es un error por favor contactá a algunx admin. Gracias."
			)
			return LADATA
		else:
			raise e
	if chat_member.status == 'kicked' or chat_member.status == 'left':
		await update.message.reply_text(
			"Lo siento debés volver al grupo de usuarixs para poder enviar mensajes. "
			"Si creés que esto es un error por favor contactá a algunx admin. Gracias."
		)
		return LADATA
		
	# Guardamos el msj para envío posconfirmación
	context.user_data["ladata"] = update.message
	
	await update.message.reply_text(
		"*️⃣ ¿¿Estás segurx que querés enviar ese mensaje??\n"
		". . . . . . . . . . . . . . | /SI | /NO | . . . . . . . . . . ."
	)
	return CONFIRMA

async def si(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
	message = context.user_data["ladata"]
	await message.copy(ANONI_CHANNEL)
	del context.user_data["ladata"]
	await update.message.reply_text("✅ ¡Mensaje enviado!\nQuedo aquí a disposición para enviar más data.")
	return LADATA

async def no(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
	del context.user_data["ladata"]
	await update.message.reply_text("⛔️ Ok, mensaje cancelado.\nCuando quieras podés volver a enviarme más data.")
	return LADATA

async def cancelar(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
	context.user_data.pop("ladata", None)
	await update.message.reply_text("⛔️ Ok ... * cri cri *")
	return LADATA

async def post_init(application: Application) -> None:
	print(f'Login de bot correcto:\n- {application.bot.name} "{application.bot.first_name}"')
	print(f'Link de bot updates:\n- {application.bot.base_url}/getUpdates')
	
	# Chequiamos que el bot pertenezca al grupo de usuarixs (para poder verificar usuarixs que pertenezcan)
	try:
		# ChatMember: https://docs.python-telegram-bot.org/en/stable/telegram.chatmember.html
		chat_member = await application.bot.get_chat_member(USERS_GROUP, application.bot.id)
		if chat_member.status == 'kicked' or chat_member.status == 'left':
			print(f'¡¡Cuidado el bot no pertenece al chat de usuarixs ({USERS_GROUP})!!')
		print(f'OK: el bot pertenece al chat de usuarixs ({USERS_GROUP})')
	except BadRequest as e:
		if str(e) == 'Chat not found':
			print(f'¡¡Cuidado el chat de usuarixs ({USERS_GROUP}) no se encontró o el bot no pertenece a él!!')
		else:
			raise e
	except Forbidden:
			print(f'¡¡Cuidado el bot no pertenece o fue kickeado del chat de usuarixs ({USERS_GROUP})!!')
	
	# Chequiamos que el bot pertenezca al canal (para poder enviar los mensajes)
	try:
		# ChatMember: https://docs.python-telegram-bot.org/en/stable/telegram.chatmember.html
		chat_member = await application.bot.get_chat_member(ANONI_CHANNEL, application.bot.id)
		if chat_member.status == 'kicked' or chat_member.status == 'left':
			print(f'¡¡Cuidado el bot no pertenece al canal de mensajes ({ANONI_CHANNEL})!!')
		print(f'OK: el bot pertenece al canal de mensajes ({ANONI_CHANNEL})')
	except BadRequest as e:
		if str(e) == 'Chat not found':
			print(f'¡¡Cuidado el canal de mensajes ({ANONI_CHANNEL}) no se encontró o el bot no pertenece a él!!')
		else:
			raise e
	except Forbidden:
			print(f'¡¡Cuidado el bot no pertenece o fue kickeado del canal de mensajes ({ANONI_CHANNEL})!!')


def main() -> None:
    """Run the bot."""
    # Create the Application and pass it your bot's token.
    # telegram.ext.Application:
    # https://docs.python-telegram-bot.org/en/stable/telegram.ext.application.html#telegram.ext.Application
    application = Application.builder().token(BOT_TOKEN).post_init(post_init).build()

    # Add conversation handler with the states
    # filters Module: https://docs.python-telegram-bot.org/en/stable/telegram.ext.filters.html
    conv_handler = ConversationHandler(
        entry_points=[
			CommandHandler("start", start),
			MessageHandler(filters.ChatType.PRIVATE & ~filters.COMMAND, ladata)
		],
        states={
            LADATA: [MessageHandler(filters.ChatType.PRIVATE & ~filters.COMMAND, ladata)],
            CONFIRMA: [
				CommandHandler("si", si),
				CommandHandler("no", no)
			],
        },
        fallbacks=[
			CommandHandler("start", start),
			CommandHandler("cancelar", cancelar),
			CommandHandler("desactivar", desactivar, filters.User(username=SUDO_USERS)),
			CommandHandler("activar", activar, filters.User(username=SUDO_USERS))
		]
    )

    application.add_handler(conv_handler)
    
    application.bot_data["activado"]=True
    
    # Run the bot until the user presses Ctrl-C
    application.run_polling(allowed_updates=Update.ALL_TYPES)


if __name__ == "__main__":
    main()
