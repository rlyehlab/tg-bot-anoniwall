# no usamos alpine porque trae muchos problemas para buildear
# mirar https://pythonspeed.com/articles/alpine-docker-python/
# set base image (host OS)
# https://hub.docker.com/_/python/
FROM python:3-slim

WORKDIR /usr/src/app

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY . .

# -u para que flushee los print() a output/log, sino no se ve nada
CMD [ "python", "-u", "./bot.py" ]
